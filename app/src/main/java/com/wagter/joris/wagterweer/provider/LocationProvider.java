package com.wagter.joris.wagterweer.provider;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.wagter.joris.wagterweer.activity.MainActivity;
import com.wagter.joris.wagterweer.R;
import com.wagter.joris.wagterweer.util.LocationAware;

/**
 * To provide the current location of the device.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class LocationProvider implements LocationListener
{
    /**
     * The location aware object.
     */
    private LocationAware locationAware;

    /**
     * The main activity of the app.
     */
    private MainActivity activity;

    /**
     * LocationProvider constructor.
     *
     * @param activity the main activity of the application.
     * @param locationAware the location aware object.
     */
    public LocationProvider( MainActivity activity, LocationAware locationAware )
    {
        this.activity = activity;
        this.locationAware = locationAware;
    }

    /**
     * Request a location update.
     */
    public void requestUpdate()
    {
        Location cachedLocation = loadFromCache();
        if (cachedLocation != null) {
            locationAware.setLocation( cachedLocation );
            Toast.makeText( activity, R.string.cached_location, Toast.LENGTH_SHORT ).show();
        }

        if ( ContextCompat.checkSelfPermission(
                activity.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission( activity.getApplicationContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED
                ) {

            ActivityCompat.requestPermissions(
                    activity,
                    new String[] {
                            android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    101
            );

        }

        try {
            LocationManager locationManager = (LocationManager) activity.getSystemService( Context.LOCATION_SERVICE );
            assert locationManager != null;
            activity.setLocation( locationManager.getLastKnownLocation( LocationManager.GPS_PROVIDER ) );

            locationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 5000, 5, this );

        } catch ( SecurityException e ) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged( Location location )
    {
        locationAware.setLocation( location );
        Toast.makeText( activity, R.string.location_changed, Toast.LENGTH_SHORT ).show();
        saveToCache(location);
    }

    @Override
    public void onStatusChanged( String provider, int status, Bundle extras )
    {
        if (android.location.LocationProvider.AVAILABLE == status) {
            Toast.makeText( activity, R.string.location_status_changed, Toast.LENGTH_LONG ).show();
            requestUpdate();
        }
    }

    @Override
    public void onProviderEnabled( String provider )
    {
    }

    @Override
    public void onProviderDisabled( String provider )
    {
        Toast.makeText( activity, R.string.enable_location, Toast.LENGTH_SHORT ).show();

        Location cachedLocation = loadFromCache();
        if (cachedLocation != null) {
            locationAware.setLocation( cachedLocation );

            Toast.makeText( activity, R.string.cached_location, Toast.LENGTH_SHORT ).show();
        }
    }

    /**
     * Load a saved location from cache.
     *
     * @return a saved location from cache.
     */
    private Location loadFromCache()
    {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        double latitude = (double) sharedPref.getFloat(activity.getString(R.string.latitude_key), 0.0f);
        double longitude = (double) sharedPref.getFloat(activity.getString(R.string.longitude_key), 0.0f);

        if (latitude != 0.0d && longitude != 0.0d) {
            Location location = new Location("");

            location.setLatitude( latitude );
            location.setLongitude( longitude );

            return location;
        }

        return null;

    }

    /**
     * Save a location to the cache.
     *
     * @param location the location to save to the cache.
     */
    private void saveToCache( Location location )
    {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putFloat(activity.getString(R.string.latitude_key), (float) location.getLatitude());
        editor.putFloat(activity.getString(R.string.longitude_key), (float) location.getLongitude());

        editor.apply();

    }
}
