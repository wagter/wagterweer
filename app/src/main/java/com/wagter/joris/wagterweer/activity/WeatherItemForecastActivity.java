package com.wagter.joris.wagterweer.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.wagter.joris.wagterweer.R;

/**
 * The activity to display weather forecast information.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class WeatherItemForecastActivity extends AppCompatActivity
{

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_weather_item_forecast );
    }
}
