package com.wagter.joris.wagterweer.util;

import android.location.Location;

/**
 * Compare two locations.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class CompareLocations
{
    /**
     * If the locations are the same.
     */
    private boolean same;

    /**
     * CompareLocations constructor.
     *
     * @param loc1 location one.
     * @param loc2 location two.
     */
    public CompareLocations( Location loc1, Location loc2 )
    {
        same = loc1 != null && loc2 != null && ( loc1.getLatitude() == loc2.getLatitude() ) && ( loc1.getLongitude() == loc2.getLongitude() );
    }

    /**
     * True if locations are different, false if they are the same.
     *
     * @return if locations are different.
     */
    public boolean different()
    {
        return !same;
    }
}
