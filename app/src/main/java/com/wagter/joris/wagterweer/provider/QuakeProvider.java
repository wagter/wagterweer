package com.wagter.joris.wagterweer.provider;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.wagter.joris.wagterweer.model.Quake;
import com.wagter.joris.wagterweer.util.QuakeAware;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * To provide earthquake data from the servers of KNMI.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class QuakeProvider extends JSONProvider
{
    /**
     * The list of quakes retrieved.
     */
    private List< Quake > quakeList;

    /**
     * The list of quake aware objects to set the retrieved quake list to.
     */
    private List< QuakeAware > quakeAwareList;

    /**
     * QuakeProvider constructor.
     *
     * @param quakeAwareList list of quake aware objects to set the retrieved quake list to.
     */
    public QuakeProvider( List< QuakeAware > quakeAwareList )
    {
        this.quakeList = new ArrayList<>();
        this.quakeAwareList = quakeAwareList;
    }


    @SuppressLint( "StaticFieldLeak" )
    public void requestUpdate()
    {
        new AsyncTask< Void, Void, Void >()
        {

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground( Void... params )
            {
                try {
                    JSONArray tectonicQuakes = getJSON( new URL( "http://cdn.knmi.nl/knmi/map/page/seismologie/all_tectonic.json" ) )
                            .getJSONArray( "events" );
                    JSONArray inducedQuakes = getJSON( new URL( "http://cdn.knmi.nl/knmi/map/page/seismologie/all_induced.json" ) )
                            .getJSONArray( "events" );

                    System.out.println(tectonicQuakes);
                    System.out.println(inducedQuakes);

                    for ( int i = 0; i < tectonicQuakes.length() && i < 16; i++ ) {
                        quakeList.add( createQuakeFromJSON( (JSONObject) tectonicQuakes.get( i ) ) );
                    }
                    for ( int i = 0; i < inducedQuakes.length() && i < 16; i++ ) {
                        quakeList.add( createQuakeFromJSON( (JSONObject) inducedQuakes.get( i ) ) );
                    }

                } catch ( IOException | JSONException | ParseException e ) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute( Void Void )
            {
                for ( QuakeAware quakeAware : quakeAwareList ) {
                    quakeAware.setQuakes( quakeList );
                }
            }

        }.execute();
    }

    /**
     * Create a Quake object from the JSON retrieved.
     *
     * @param jsonQuake the quake data in JSON format.
     * @return the Quake object created from the data in the JSON object.
     *
     * @throws JSONException if the JSON is malformed
     * @throws ParseException if the parsing of the JSON has failed.
     */
    private Quake createQuakeFromJSON( JSONObject jsonQuake ) throws JSONException, ParseException
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss", Locale.ENGLISH );
        cal.setTime( sdf.parse( jsonQuake.getString( "date" ) + " " + jsonQuake.getString( "time" ) ) );// all done

        return new Quake()
        {{
            setDateTime( cal.getTime() );
            setDepth( jsonQuake.getDouble( "depth" ) );
            setEvaluationMode( jsonQuake.getString( "evaluationMode" ) );
            setLat( jsonQuake.getDouble( "lat" ) );
            setLng( jsonQuake.getDouble( "lon" ) );
            setMagnitude( jsonQuake.getDouble( "mag" ) );
            setPlace( jsonQuake.getString( "place" ) );
            setType( jsonQuake.getString( "type" ) );
        }};
    }
}
