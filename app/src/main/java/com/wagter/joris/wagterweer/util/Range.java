package com.wagter.joris.wagterweer.util;

/**
 * To define a range between two integers.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Range
{
    /**
     * The lowest number of the range.
     */
    private int min;

    /**
     * The highest number of the range.
     */
    private int max;

    /**
     * Range constructor.
     *
     * @param min lowest number of the range.
     * @param max highest number of the range.
     */
    public Range( int min, int max )
    {
        this.min = min;
        this.max = max;
    }

    /**
     * Check if the specified number is in the range.
     *
     * @param number the number to check if is in the range.
     * @return if the specified number is in the range.
     */
    public boolean inRange( int number )
    {
        return ( number >= min && number <= max );
    }
}
