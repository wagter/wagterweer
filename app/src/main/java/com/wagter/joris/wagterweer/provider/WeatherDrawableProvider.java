package com.wagter.joris.wagterweer.provider;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.wagter.joris.wagterweer.R;
import com.wagter.joris.wagterweer.model.Forecast;
import com.wagter.joris.wagterweer.util.Range;
import com.wagter.joris.wagterweer.model.Weather;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provides images and icons matching a weather condition.
 *
 * @link https://openweathermap.org/weather-conditions
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class WeatherDrawableProvider
{
    /**
     * A map containing the String representations of a weather condition and their matching icon Drawable resource id.
     */
    private Map< String, Integer > iconMap = createIconMap();

    /**
     * A map containing the String representations of a weather condition and their matching image Drawable resource id.
     */
    private Map< String, Integer > imageMap = createImageMap();

    /**
     * A list with RangeIconPair objects  to match a numeric weather condition with a String representation of the same weather condition.
     */
    private List< RangeIconPair > conditionIconList = createConditionIconList();

    /**
     * The application Context.
     */
    private Context context;

    /**
     * WeatherDrawableProvider constructor.
     *
     * @param context the application context.
     */
    public WeatherDrawableProvider( Context context )
    {
        this.context = context;
    }

    /**
     * Get an icon matching the current weather conditions.
     *
     * @param weather the Weather object to determine the conditions.
     * @return the icon Drawable that matches the current weather condition.
     */
    public Drawable getIconByWeather( Weather weather )
    {
        Date now = new Date();
        boolean isDay = now.after( weather.getSunrise() ) && now.before( weather.getSunset() );
        return ContextCompat.getDrawable( context, getIconId( weather.getCondition(), isDay ) );
    }

    /**
     * Get an image matching the current weather conditions.
     *
     * @param weather the Weather object to determine the conditions.
     * @return the image Drawable that matches the current weather condition.
     */
    public Drawable getImageByWeather( Weather weather )
    {
        return ContextCompat.getDrawable( context, getImageId( weather.getCondition() ) );
    }

    /**
     * Get an icon matching the forecast weather conditions.
     *
     * @param forecast the Forecast object to determine the conditions.
     * @return the icon Drawable that matches the forecast weather condition.
     */
    public Drawable getIconByForecast( Forecast forecast )
    {
        return ContextCompat.getDrawable(
                context,
                getIconId( forecast.getCondition(), true )
        );
    }

    /**
     * Get the icon Drawable resource id matching a weather condition.
     *
     * @param condition the weather condition
     * @param isDay     should it be an icon for day? (false for night icon).
     * @return the icon Drawable resource id matching the weather condition.
     */
    private int getIconId( int condition, boolean isDay )
    {
        StringBuilder stringBuilder = new StringBuilder();

        for ( RangeIconPair pair : conditionIconList ) {
            Range range = pair.getRange();
            if ( range.inRange( condition ) ) {
                if ( !isDay ) {
                    stringBuilder.append( "nt_" );
                }
                stringBuilder.append( pair.getIcon() );
                return iconMap.get( stringBuilder.toString() );
            }
        }

        return iconMap.get( "unknown" );
    }

    /**
     * Get the image Drawable resource id matching a weather condition.
     *
     * @param condition the weather condition
     * @return the image Drawable resource id matching the weather condition.
     */
    private int getImageId( int condition )
    {
        StringBuilder stringBuilder = new StringBuilder();
        for ( RangeIconPair pair : conditionIconList ) {
            Range range = pair.getRange();
            if ( range.inRange( condition ) ) {

                stringBuilder.append( pair.getIcon() );
                return imageMap.get( stringBuilder.toString() );
            }
        }

        stringBuilder.append( "unknown" );
        return iconMap.get( stringBuilder.toString() );
    }


    /**
     * Create a map to map an image Drawable resource id to a String representation of a weather condition.
     *
     * @return a map containing the String representations of a weather condition and their matching image Drawable resource id.
     */
    private Map< String, Integer > createImageMap()
    {
        return new HashMap< String, Integer >()
        {{
            put( "tstorms", R.drawable.img_tstorms );
            put( "chancerain", R.drawable.img_chancerain );
            put( "rain", R.drawable.img_rain );
            put( "snow", R.drawable.img_snow );
            put( "fog", R.drawable.img_fog );
            put( "clear", R.drawable.img_clear );
            put( "mostlycloudy", R.drawable.img_mostlycloudy );
            put( "mostlysunny", R.drawable.img_mostlysunny );
            put( "cloudy", R.drawable.img_cloudy );
            put( "unknown", R.drawable.img_unknown );
        }};
    }

    /**
     * Create a map to map an icon Drawable resource id to a String representation of a weather condition.
     *
     * @return a map containing the String representations of a weather condition and their matching icon Drawable resource id.
     */
    private Map< String, Integer > createIconMap()
    {
        return new HashMap< String, Integer >()
        {{
            // day icons
            put( "chancerain", R.drawable.ic_chancerain );
            put( "clear", R.drawable.ic_clear );
            put( "cloudy", R.drawable.ic_cloudy );
            put( "fog", R.drawable.ic_fog );
            put( "mostlycloudy", R.drawable.ic_mostlycloudy );
            put( "mostlysunny", R.drawable.ic_mostlysunny );
            put( "rain", R.drawable.ic_rain );
            put( "snow", R.drawable.ic_snow );
            put( "tstorms", R.drawable.ic_tstorms );
            put( "unknown", R.drawable.ic_unknown );

            // night icons
            put( "nt_chancerain", R.drawable.ic_nt_chancerain );
            put( "nt_clear", R.drawable.ic_nt_clear );
            put( "nt_cloudy", R.drawable.ic_nt_cloudy );
            put( "nt_fog", R.drawable.ic_nt_fog );
            put( "nt_mostlycloudy", R.drawable.ic_nt_mostlycloudy );
            put( "nt_mostlysunny", R.drawable.ic_nt_mostlysunny );
            put( "nt_rain", R.drawable.ic_nt_rain );
            put( "nt_snow", R.drawable.ic_nt_snow );
            put( "nt_tstorms", R.drawable.ic_nt_tstorms );
        }};
    }

    /**
     * Create a list with RangeIconPair objects to match a numeric weather condition with a String representation of the same weather condition.
     *
     * @return a list with RangeIconPair objects  to match a numeric weather condition with a String representation of the same weather condition.
     */
    private List< RangeIconPair > createConditionIconList()
    {
        return new ArrayList< RangeIconPair >()
        {{
            add( new RangeIconPair( new Range( 200, 299 ), "tstorms" ) );
            add( new RangeIconPair( new Range( 300, 399 ), "chancerain" ) );
            add( new RangeIconPair( new Range( 500, 599 ), "rain" ) );
            add( new RangeIconPair( new Range( 600, 699 ), "snow" ) );
            add( new RangeIconPair( new Range( 700, 799 ), "fog" ) );
            add( new RangeIconPair( new Range( 800, 800 ), "clear" ) );
            add( new RangeIconPair( new Range( 801, 801 ), "mostlysunny" ) );
            add( new RangeIconPair( new Range( 802, 803 ), "mostlycloudy" ) );
            add( new RangeIconPair( new Range( 804, 804 ), "cloudy" ) );
        }};
    }

    /**
     * A class to match a numeric weather condition with a String representation of the same weather condition.
     */
    private class RangeIconPair
    {
        /**
         * The numeric range for the weather condition.
         */
        private Range range;

        /**
         * The String representation of the weather condition.
         */
        private String icon;

        /**
         * RangeIconPair constructor.
         *
         * @param range numeric range for the weather condition.
         * @param icon  String representation of the weather condition.
         */
        RangeIconPair( Range range, String icon )
        {
            this.range = range;
            this.icon = icon;
        }

        /**
         * Get the numeric range for the weather condition.
         *
         * @return the numeric range for the weather condition.
         */
        Range getRange()
        {
            return range;
        }

        /**
         * Get the String representation of the weather condition.
         *
         * @return the String representation of the weather condition.
         */
        String getIcon()
        {
            return icon;
        }
    }
}
