package com.wagter.joris.wagterweer.provider;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;

import com.google.gson.reflect.TypeToken;
import com.iainconnor.objectcache.CacheManager;
import com.iainconnor.objectcache.DiskCache;
import com.wagter.joris.wagterweer.BuildConfig;
import com.wagter.joris.wagterweer.model.Forecast;
import com.wagter.joris.wagterweer.model.Weather;
import com.wagter.joris.wagterweer.util.WeatherAware;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * WeatherProvider
 *
 * Class to provide weather from the (openweathermap.org) API.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class WeatherProvider extends JSONProvider
{
    /**
     * The object to set the weather to.
     */
    private WeatherAware weatherAwareObject;

    /**
     * The raw weather data.
     */
    private JSONObject data;

    /**
     * The raw forecast data.
     */
    private JSONObject forecastData;

    /**
     * The application context.
     */
    private Context context;

    /**
     * The filesystem cache manager.
     */
    private CacheManager cacheManager;

    /**
     * WeatherProvider constructor.
     *
     * @param weatherAwareObject object to set the weather to.
     * @param context application context.
     */
    public WeatherProvider( WeatherAware weatherAwareObject, Context context )
    {
        this.weatherAwareObject = weatherAwareObject;
        this.context = context;
    }

    /**
     * Request a weather update for a given location.
     *
     * @param location the location to retrieve the weather.
     * @param swipeRefreshLayout the swipeRefreshLayout to start and stop loading animation.
     */
    public void requestUpdate( final Location location, final SwipeRefreshLayout swipeRefreshLayout )
    {
        UpdateTask task = new UpdateTask( this, location, swipeRefreshLayout );
        task.execute();
    }

    /**
     * Save the current weather data to the cache.
     */
    private void saveToCache()
    {
        CacheManager cacheManager = getCacheManager();
        if ( cacheManager != null && data != null ) {
            cacheManager.put( "latest_weather", createFromJson( data ) );
        }
    }

    /**
     * Get the CacheManager object for the application.
     * @return the CacheManager object for the application.
     */
    private CacheManager getCacheManager()
    {
        if (cacheManager == null) {

            String cachePath = context.getCacheDir().getPath();
            File cacheFile = new File( cachePath + File.separator + "wagterweer" );

            try {
                DiskCache diskCache = new DiskCache( cacheFile, BuildConfig.VERSION_CODE, 1024 * 1024 * 10 );
                cacheManager = CacheManager.getInstance( diskCache );
            } catch ( IOException e ) {
                e.printStackTrace();
                cacheManager = null;
            }
        }

        return cacheManager;
    }

    /**
     * Load Weather object from filesystem cache.
     *
     * @return the Weather object loaded from the cache.
     */
    public Weather loadFromCache()
    {
        CacheManager cacheManager = getCacheManager();
        if ( cacheManager != null ) {
            Type weatherType = new TypeToken< Weather >()
            {
            }.getType();
            return (Weather) cacheManager.get( "latest_weather", Weather.class, weatherType );
        }
        return null;
    }

    /**
     * Create a Weather object from JSON data.
     *
     * @param json the JSON data to create the Weather object from.
     * @return the newly created Weather object.
     */
    private Weather createFromJson( JSONObject json )
    {
        System.out.println( json );
        try {
            Weather weather = new Weather();
            weather.setPlace( json.getString( "name" ) );
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis( (long) json.getInt( "dt" ) * 1000 );
            weather.setDate( date.getTime() );

            JSONObject jsonWeather = json
                    .getJSONArray( "weather" )
                    .getJSONObject( 0 );

            weather.setCondition( jsonWeather.getInt( "id" ) );
            weather.setDescription( jsonWeather.getString( "description" ) );

            JSONObject jsonMain = json.getJSONObject( "main" );
            weather.setTemperature( jsonMain.getDouble( "temp" ) );
            weather.setTemperatureMin( jsonMain.getDouble( "temp_min" ) );
            weather.setTemperatureMax( jsonMain.getDouble( "temp_max" ) );
            weather.setPressure( jsonMain.getInt( "pressure" ) );
            weather.setHumidity( jsonMain.getInt( "humidity" ) );

            JSONObject jsonWind = json.getJSONObject( "wind" );
            weather.setWindSpeed( jsonWind.getDouble( "speed" ) );
            weather.setWindDegrees( jsonWind.getInt( "deg" ) );

            JSONObject jsonSys = json.getJSONObject( "sys" );

            Calendar sunrise = Calendar.getInstance();
            sunrise.setTimeInMillis( (long) jsonSys.getInt( "sunrise" ) * 1000 );
            weather.setSunrise( sunrise.getTime() );

            Calendar sunset = Calendar.getInstance();
            sunset.setTimeInMillis( (long) jsonSys.getInt( "sunset" ) * 1000 );
            weather.setSunset( sunset.getTime() );

            return weather;

        } catch ( JSONException e ) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create a Forecast list from JSON data.
     *
     * @param json the JSON data to create the Weather object from.
     * @return the newly created Forecast list.
     */
    private List< Forecast > createForecastsFromJSON( JSONObject json ) throws JSONException
    {
        List< Forecast > forecastList = new ArrayList<>();

        JSONArray forecasts = json.getJSONArray( "list" );
        Calendar date = Calendar.getInstance();

        for ( int i = 0; i < forecasts.length(); i++ ) {

            Forecast forecast = new Forecast();
            JSONObject jsonForecast = forecasts.getJSONObject( i );
            date.setTimeInMillis( (long) jsonForecast.getInt( "dt" ) * 1000 );
            forecast.setDate( date.getTime() );

            JSONObject jsonTemp = jsonForecast.getJSONObject( "temp" );
            forecast.setTemperatureMin( jsonTemp.getDouble( "min" ) );
            forecast.setTemperatureMax( jsonTemp.getDouble( "max" ) );

            JSONArray weatherArray = jsonForecast.getJSONArray( "weather" );
            JSONObject weatherObject = (JSONObject) weatherArray.get( 0 );
            forecast.setCondition( weatherObject.getInt( "id" ) );

            forecastList.add( forecast );
        }

        return forecastList;
    }

    /**
     * The asynchronous task to update the weather from the API.
     */
    @SuppressLint("StaticFieldLeak")
    private class UpdateTask extends AsyncTask< Void, Void, Void >
    {
        final WeatherProvider weatherProvider;
        final Location location;
        final SwipeRefreshLayout swipeRefreshLayout;

        UpdateTask( final WeatherProvider weatherProvider, final Location location, final SwipeRefreshLayout swipeRefreshLayout )
        {
            this.weatherProvider = weatherProvider;
            this.location = location;
            this.swipeRefreshLayout = swipeRefreshLayout;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground( Void... params )
        {
            try {

                String url1 = "http://api.openweathermap.org/data/2.5/weather" +
                        "?lat=" +
                        location.getLatitude() +
                        "&lon=" +
                        location.getLongitude() +
                        "&units=metric" +
                        "&lang=nl" +
                        "&appid=09a6c179fbb39a0f187bfd55a3b2d2f5";

                data = weatherProvider.getJSON( new URL( url1 ) );

                String url2 = "http://api.openweathermap.org/data/2.5/forecast/daily" +
                        "?lat=" +
                        location.getLatitude() +
                        "&lon=" + location.getLongitude() +
                        "&units=metric" +
                        "&lang=nl" +
                        "&appid=09a6c179fbb39a0f187bfd55a3b2d2f5";

                forecastData = weatherProvider.getJSON( new URL( url2 ) );

                System.out.println( forecastData.getJSONArray( "list" ) );

                if ( swipeRefreshLayout != null ) {
                    swipeRefreshLayout.setRefreshing( false );
                }

                if ( data.getInt( "cod" ) != 200 ) {
                    return null;
                }
            } catch ( Exception e ) {
                System.out.println( "Exception " + e.getMessage() );
                return null;
            }

            return null;
        }

        @Override
        protected void onPostExecute( Void Void )
        {
            if ( data != null ) {
                saveToCache();
                Weather weather = weatherProvider.createFromJson( data );

                if ( weather != null ) {

                    try {
                        weather.setForecastList( weatherProvider.createForecastsFromJSON( forecastData ) );
                    } catch ( JSONException e ) {
                        e.printStackTrace();
                    }

                    weatherAwareObject.setWeather( weather );
                }
            }
        }
    }
}
