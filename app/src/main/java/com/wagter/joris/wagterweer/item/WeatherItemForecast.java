package com.wagter.joris.wagterweer.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wagter.joris.wagterweer.R;
import com.wagter.joris.wagterweer.converter.DayOfWeekConverter;
import com.wagter.joris.wagterweer.model.Forecast;
import com.wagter.joris.wagterweer.model.Weather;
import com.wagter.joris.wagterweer.provider.WeatherDrawableProvider;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * To display three forecast items.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class WeatherItemForecast extends AbstractWeatherItem
{
    /**
     * The text views for the forecast days.
     */
    private TextView forecastDay1, forecastDay2, forecastDay3;

    /**
     * The text views for the forecast minimum temperatures.
     */
    private TextView forecastTempMin1, forecastTempMin2, forecastTempMin3;

    /**
     * The text views for the forecast maximum temperatures.
     */
    private TextView forecastTempMax1, forecastTempMax2, forecastTempMax3;

    /**
     * The image views for the forecast condition icons.
     */
    private ImageView forecastIcon1, forecastIcon2, forecastIcon3;

    /**
     * The drawable provider to get the icons for the forecast conditions.
     */
    private WeatherDrawableProvider iconProvider;

    /**
     * WeatherItemForecast constructor.
     *
     * @param context the application context.
     * @param weather the weather object containing the forecasts.
     */
    public WeatherItemForecast( Context context, Weather weather )
    {
        super( context, weather );
        iconProvider = new WeatherDrawableProvider( context );

    }

    @Override
    public View inflateView( LayoutInflater inflater, View convertView, ViewGroup root, boolean attachToRoot )
    {
        if ( convertView == null ) {
            convertView = inflater.inflate( R.layout.activity_weather_item_forecast, root, attachToRoot );
        }

        forecastDay1 = convertView.findViewById( R.id.forecastDay1 );
        forecastDay2 = convertView.findViewById( R.id.forecastDay2 );
        forecastDay3 = convertView.findViewById( R.id.forecastDay3 );

        forecastTempMin1 = convertView.findViewById( R.id.forecastTempMin1 );
        forecastTempMin2 = convertView.findViewById( R.id.forecastTempMin2 );
        forecastTempMin3 = convertView.findViewById( R.id.forecastTempMin3 );

        forecastTempMax1 = convertView.findViewById( R.id.forecastTempMax1 );
        forecastTempMax2 = convertView.findViewById( R.id.forecastTempMax2 );
        forecastTempMax3 = convertView.findViewById( R.id.forecastTempMax3 );

        forecastIcon1 = convertView.findViewById( R.id.forecastIcon1 );
        forecastIcon2 = convertView.findViewById( R.id.forecastIcon2 );
        forecastIcon3 = convertView.findViewById( R.id.forecastIcon3 );

        return convertView;
    }

    @Override
    public void updateViews()
    {
        if ( weather == null ) {
            return;
        }

        if ( weather.getForecastList().size() == 0 ) {
            return;
        }

        Forecast forecast1 = weather.getForecastList().get( 0 );
        Forecast forecast2 = weather.getForecastList().get( 1 );
        Forecast forecast3 = weather.getForecastList().get( 2 );

        if ( forecastDay1 != null && forecastDay2 != null && forecastDay3 != null ) {
            SimpleDateFormat df = new SimpleDateFormat( "E", Locale.ENGLISH );
            DayOfWeekConverter dow = new DayOfWeekConverter();
            forecastDay1.setText( dow.get(df.format( forecast1.getDate() )) );
            forecastDay2.setText( dow.get(df.format( forecast2.getDate() )) );
            forecastDay3.setText( dow.get(df.format( forecast3.getDate() )) );
        }

        if ( forecastIcon1 != null && forecastIcon2 != null && forecastIcon3 != null ) {
            forecastIcon1.setImageDrawable( iconProvider.getIconByForecast( forecast1 ) );
            forecastIcon2.setImageDrawable( iconProvider.getIconByForecast( forecast2 ) );
            forecastIcon3.setImageDrawable( iconProvider.getIconByForecast( forecast3 ) );
        }

        if ( forecastTempMin1 != null && forecastTempMin2 != null && forecastTempMin3 != null ) {
            forecastTempMin1.setText( context.getString( R.string.temperature, "" + forecast1.getTemperatureMin() ) );
            forecastTempMin2.setText( context.getString( R.string.temperature, "" + forecast2.getTemperatureMin() ) );
            forecastTempMin3.setText( context.getString( R.string.temperature, "" + forecast3.getTemperatureMin() ) );
        }

        if ( forecastTempMax1 != null && forecastTempMax2 != null && forecastTempMax3 != null ) {
            forecastTempMax1.setText( context.getString( R.string.temperature, "" + forecast1.getTemperatureMax() ) );
            forecastTempMax2.setText( context.getString( R.string.temperature, "" + forecast2.getTemperatureMax() ) );
            forecastTempMax3.setText( context.getString( R.string.temperature, "" + forecast3.getTemperatureMax() ) );
        }
    }
}
