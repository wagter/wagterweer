package com.wagter.joris.wagterweer.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wagter.joris.wagterweer.R;
import com.wagter.joris.wagterweer.converter.WindSpeedConverter;
import com.wagter.joris.wagterweer.converter.WindZoneConverter;
import com.wagter.joris.wagterweer.model.Weather;

/**
 * To display information about wind direction and speed.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class WeatherItemWind extends AbstractWeatherItem
{
    /**
     * The image view to display a wind rose.
     */
    private ImageView windRoseView;

    /**
     * A text view to display the wind speed.
     */
    private TextView windSpeedView;

    /**
     * A text view to display the wind direction.
     */
    private TextView windDirectionView;

    /**
     * WeatherItemWind constructor.
     *
     * @param context the application context.
     * @param weather the weather model.
     */
    public WeatherItemWind( Context context, Weather weather )
    {
        super( context, weather );
    }

    @Override
    public View inflateView( LayoutInflater inflater, View convertView, ViewGroup root, boolean attachToRoot )
    {
        if ( convertView == null ) {
            convertView = inflater.inflate( R.layout.activity_weather_item_wind, root, attachToRoot );
        }

        windRoseView = convertView.findViewById( R.id.windRose );
        windSpeedView = convertView.findViewById( R.id.windSpeed );
        windDirectionView = convertView.findViewById( R.id.windDirection );

        return convertView;
    }

    @Override
    public void updateViews()
    {
        if ( weather == null ) {
            return;
        }

        if ( windRoseView != null ) {
            windRoseView.setRotation( ( weather.getWindDegrees() - 180 ) % 360 );
        }

        if ( windSpeedView != null ) {
            WindSpeedConverter speed = new WindSpeedConverter( (int) Math.round( weather.getWindSpeed() ) );
            windSpeedView.setText( speed.toString() );
        }

        if ( windDirectionView != null ) {
            WindZoneConverter zone = new WindZoneConverter( weather.getWindDegrees() );
            windDirectionView.setText( zone.toString() );
        }
    }
}
