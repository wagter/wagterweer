package com.wagter.joris.wagterweer.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;
import com.wagter.joris.wagterweer.R;
import com.wagter.joris.wagterweer.model.Quake;
import com.wagter.joris.wagterweer.map.QuakeItem;
import com.wagter.joris.wagterweer.util.QuakeAware;
import com.wagter.joris.wagterweer.provider.QuakeProvider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * The activity to display quake information.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class QuakeActivity extends FragmentActivity implements OnMapReadyCallback, QuakeAware, GoogleMap.OnMarkerClickListener
{
    /**
     * The map to display the quakes on.
     */
    private GoogleMap mMap;

    /**
     * The cluster manager (clusters markers that are near when zoomed out.)
     */
    private ClusterManager< QuakeItem > clusterManager;

    /**
     * The list of Quake objects.
     */
    private List< Quake > quakes;

    /**
     * The text view to display detailed info about the earthquakes.
     */
    private TextView placeView, quakeDateView, magnitudeView, depthView;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        String languageToLoad = "nl_NL";
        Locale locale = new Locale( languageToLoad );
        Locale.setDefault( locale );
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration( config,
                getBaseContext().getResources().getDisplayMetrics() );

        setContentView( R.layout.activity_quake );

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById( R.id.map );
        mapFragment.getMapAsync( this );

        placeView = findViewById( R.id.place );
        quakeDateView = findViewById( R.id.quakeDate );
        magnitudeView = findViewById( R.id.magnitude );
        depthView = findViewById( R.id.depth );
    }

    @Override
    public void onMapReady( GoogleMap googleMap )
    {
        List< QuakeAware > quakeAwareList = new ArrayList<>();
        quakeAwareList.add( this );
        QuakeProvider quakeProvider = new QuakeProvider( quakeAwareList );

        quakeProvider.requestUpdate();

        mMap = googleMap;
        clusterManager = new ClusterManager<>( this, mMap );

        mMap.setOnCameraIdleListener( clusterManager );
        mMap.setOnMarkerClickListener( this );

        mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.mapstyles ) );
    }

    @Override
    public void setQuakes( List< Quake > quakes )
    {
        this.quakes = quakes;

        if ( mMap == null ) {
            return;
        }

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();

        for ( int i = 0; i < quakes.size(); i++ ) {
            Quake quake = quakes.get( i );
            QuakeItem quakeItem = new QuakeItem(
                    quake.getLat(),
                    quake.getLng(),
                    "" + i,
                    "" + quake.getMagnitude()
            );
            boundsBuilder.include( quakeItem.getPosition() );
            clusterManager.addItem( quakeItem );
        }

        LatLngBounds bounds = boundsBuilder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds( bounds, 250 );
        mMap.animateCamera( cu );
    }

    @Override
    public List< Quake > getQuakes()
    {
        return null;
    }

    @Override
    public boolean onMarkerClick( Marker marker )
    {
        System.out.println( "Marker title: " + marker.getTitle() );


        try {
            int quakeIndex = Integer.parseInt( marker.getTitle() );
            Quake quake = quakes.get( quakeIndex );
            System.out.println( "Quake place: " + quake.getPlace() );

            if ( placeView != null && magnitudeView != null && depthView != null ) {
                placeView.setText( quake.getPlace() );
                SimpleDateFormat format = new SimpleDateFormat("d-M-y", Locale.ENGLISH );
                quakeDateView.setText( format.format( quake.getDateTime() ));
                magnitudeView.setText( getString( R.string.quake_magnitude, "" + quake.getMagnitude() ) );
                depthView.setText( getString( R.string.quake_depth, "" + quake.getDepth() ) );
            }

        } catch ( NumberFormatException e ) {
            e.printStackTrace();
        }

        return true;
    }
}
