package com.wagter.joris.wagterweer.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.wagter.joris.wagterweer.R;

/**
 * A dialog to show the terms and conditions of the app.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TermsDialog
{
    /**
     * The activity object.
     */
    private AppCompatActivity activity;

    /**
     * The shared preferences object.
     */
    private SharedPreferences sharedPreferences;

    /**
     * The dialog.
     */
    private AlertDialog dialog;

    /**
     * TermsDialog constructor.
     *
     * @param activity the activity to create the dialog for.
     */
    public TermsDialog( AppCompatActivity activity )
    {
        this.activity = activity;

        sharedPreferences = activity.getSharedPreferences(
                activity.getString( R.string.consents_file ), Context.MODE_PRIVATE );

        boolean accepted = sharedPreferences.getBoolean( activity.getString( R.string.terms_accepted ), false );

        if ( !accepted ) {
            dialog = createDialog();
        }
    }

    /**
     * Create the dialog.
     *
     * @return the dialog.
     */
    private AlertDialog createDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( activity );

        builder.setMessage( R.string.dialog_message )
                .setTitle( R.string.dialog_title );

        final SharedPreferences.Editor editor = sharedPreferences.edit();

        builder.setPositiveButton( R.string.accept, ( dialog1, id ) -> {
            editor.putBoolean( activity.getString( R.string.terms_accepted ), true );
            editor.apply();
            dialog1.dismiss();
        } );

        builder.setNegativeButton( R.string.deny, ( dialog12, id ) -> {
            editor.putBoolean( activity.getString( R.string.terms_accepted ), false );
            editor.apply();

            activity.finish();
            activity.moveTaskToBack( true );

        } );

        AlertDialog dialog = builder.create();

        dialog.setCancelable( false );
        dialog.setCanceledOnTouchOutside( false );

        return dialog;
    }

    /**
     * Show the dialog.
     */
    public void show()
    {
        if ( dialog != null ) {
            dialog.show();
        }
    }
}
