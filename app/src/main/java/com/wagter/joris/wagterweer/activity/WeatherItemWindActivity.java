package com.wagter.joris.wagterweer.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.wagter.joris.wagterweer.R;

/**
 * The activity to display wind information.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class WeatherItemWindActivity extends AppCompatActivity
{
    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_weather_item_wind );
    }
}
