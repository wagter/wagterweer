package com.wagter.joris.wagterweer.item;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wagter.joris.wagterweer.util.WeatherAware;

/**
 * WeatherItem is an interface to implement when creating layout elements to display weather information.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface WeatherItem extends WeatherAware
{
    /**
     * Inflate the layout
     *
     * @param inflater the layout inflater.
     * @param convertView the view to convert.
     * @param root the root view group.
     * @param attachToRoot if the inflated layout should be attached to the root view group.
     * @return the inflated view.
     */
    View inflateView(
            LayoutInflater inflater,
            View convertView,
            ViewGroup root,
            boolean attachToRoot
    );

    /**
     * Update the views.
     */
    void updateViews();
}
