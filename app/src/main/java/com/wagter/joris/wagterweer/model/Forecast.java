package com.wagter.joris.wagterweer.model;

import java.util.Date;

/**
 * A data model containing data for a weather forecast of a specific date.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Forecast
{
    /**
     * The date of the forecast.
     */
    private Date date;

    /**
     * The condition code of the forecast.
     */
    private int condition;

    /**
     * The minimum temperature of the forecast.
     */
    private double temperatureMin;

    /**
     * The maximum temperature of the forecast.
     */
    private double temperatureMax;

    /**
     * Get the date of the forecast.
     *
     * @return the date of the forecast.
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * Set the date to the forecast.
     *
     * @param date the date to set to the forecast.
     */
    public void setDate( Date date )
    {
        this.date = date;
    }

    /**
     * Get the condition code of the forecast.
     *
     * @return the condition code of the forecast.
     */
    public int getCondition()
    {
        return condition;
    }

    /**
     * Set the condition code to the forecast.
     *
     * @param condition the condition code of the forecast.
     */
    public void setCondition( int condition )
    {
        this.condition = condition;
    }

    /**
     * Get the minimum temperature of the forecast.
     *
     * @return the minimum temperature of the forecast.
     */
    public double getTemperatureMin()
    {
        return temperatureMin;
    }

    /**
     * Set the minimum temperature to the forecast.
     *
     * @param temperatureMin the minimum temperature of the forecast.
     */
    public void setTemperatureMin( double temperatureMin )
    {
        this.temperatureMin = temperatureMin;
    }

    /**
     * Get the maximum temperature of the forecast.
     *
     * @return the maximum temperature of the forecast.
     */
    public double getTemperatureMax()
    {
        return temperatureMax;
    }

    /**
     * Set the maximum temperature to the forecast.
     *
     * @param temperatureMax the maximum temperature of the forecast.
     */
    public void setTemperatureMax( double temperatureMax )
    {
        this.temperatureMax = temperatureMax;
    }
}
