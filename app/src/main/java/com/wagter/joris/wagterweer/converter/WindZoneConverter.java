package com.wagter.joris.wagterweer.converter;

import com.wagter.joris.wagterweer.util.Range;

import java.util.ArrayList;
import java.util.List;

/**
 * To translate a range of degrees to a Dutch textual representation of a wind direction.
 *
 * @link https://nl.wikipedia.org/wiki/Windstreek
 */
public class WindZoneConverter
{
    /**
     * The wind direction in degrees.
     */
    private int degrees;

    /**
     * The list containing RangeStringPair objects.
     */
    private List< RangeStringPair > windZoneList;

    /**
     * WindZoneConverter constructor.
     *
     * @param degrees the wind direction in degrees.
     */
    public WindZoneConverter( int degrees )
    {
        this.degrees = degrees;
        windZoneList = new ArrayList< RangeStringPair >()
        {{
            add( new RangeStringPair( new Range( 360, 360 ), "noord" ) );
            add( new RangeStringPair( new Range( 0, 10 ), "noord" ) );
            add( new RangeStringPair( new Range( 11, 22 ), "noord ten oosten" ) );
            add( new RangeStringPair( new Range( 23, 33 ), "noordnoordoost" ) );
            add( new RangeStringPair( new Range( 34, 44 ), "noordoost ten noorden" ) );
            add( new RangeStringPair( new Range( 45, 55 ), "noordnoordoost" ) );
            add( new RangeStringPair( new Range( 56, 67 ), "noordoost ten oosten" ) );
            add( new RangeStringPair( new Range( 68, 78 ), "oostnoordoost" ) );
            add( new RangeStringPair( new Range( 79, 89 ), "oost ten noorden" ) );
            add( new RangeStringPair( new Range( 90, 100 ), "oost" ) );
            add( new RangeStringPair( new Range( 101, 112 ), "oost ten zuiden" ) );
            add( new RangeStringPair( new Range( 113, 123 ), "oostzuidoost" ) );
            add( new RangeStringPair( new Range( 124, 134 ), "zuidoost ten oosten" ) );
            add( new RangeStringPair( new Range( 135, 145 ), "zuidoost" ) );
            add( new RangeStringPair( new Range( 146, 157 ), "zuidoost ten zuiden" ) );
            add( new RangeStringPair( new Range( 158, 168 ), "zuidzuidoost" ) );
            add( new RangeStringPair( new Range( 169, 179 ), "zuid ten oosten" ) );
            add( new RangeStringPair( new Range( 180, 190 ), "zuid" ) );
            add( new RangeStringPair( new Range( 191, 202 ), "zuid ten westen" ) );
            add( new RangeStringPair( new Range( 203, 213 ), "zuidzuidwest" ) );
            add( new RangeStringPair( new Range( 214, 224 ), "zuidwest ten zuiden" ) );
            add( new RangeStringPair( new Range( 225, 235 ), "zuidwest" ) );
            add( new RangeStringPair( new Range( 236, 247 ), "zuidwest ten westen" ) );
            add( new RangeStringPair( new Range( 248, 258 ), "westzuidwest" ) );
            add( new RangeStringPair( new Range( 259, 269 ), "west ten zuiden" ) );
            add( new RangeStringPair( new Range( 270, 280 ), "west" ) );
            add( new RangeStringPair( new Range( 281, 292 ), "west ten noorden" ) );
            add( new RangeStringPair( new Range( 293, 303 ), "westnoordwest" ) );
            add( new RangeStringPair( new Range( 304, 314 ), "noordwest ten westen" ) );
            add( new RangeStringPair( new Range( 315, 325 ), "noordwest" ) );
            add( new RangeStringPair( new Range( 326, 337 ), "noordwest ten noorden" ) );
            add( new RangeStringPair( new Range( 338, 348 ), "noordnoordwest" ) );
            add( new RangeStringPair( new Range( 349, 359 ), "noord ten westen" ) );

        }};
    }

    /**
     * A pair consisting of a Range and a String.
     */
    private class RangeStringPair
    {
        /**
         * The range object.
         */
        private Range range;

        /**
         * The String object.
         */
        private String string;

        /**
         * RangeStringPair constructor.
         *
         * @param range the Range object.
         * @param string the String object.
         */
        RangeStringPair( Range range, String string )
        {
            this.range = range;

            this.string = string;
        }

        /**
         * Get the Range object.
         *
         * @return the Range object.
         */
        Range getRange()
        {
            return range;
        }

        /**
         * Get the String object.
         *
         * @return the String object.
         */
        String getString()
        {
            return string;
        }
    }

    @Override
    public String toString()
    {
        for ( RangeStringPair pair : windZoneList ) {
            Range range = pair.getRange();
            if ( range.inRange( degrees ) ) {
                return pair.getString();
            }
        }

        return "";
    }
}
