package com.wagter.joris.wagterweer.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * A quake item for displaying on a clustered Google Map.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class QuakeItem implements ClusterItem
{
    /**
     * The position on the map.
     */
    private LatLng position;

    /**
     * The title of the item.
     */
    private String title;

    /**
     * The snippet to display on click.
     */
    private String snippet;

    /**
     * QuakeItem constructor.
     *
     * @param lat the latitude of the item.
     * @param lng the longitude of the item.
     * @param title the title of the item.
     * @param snippet the snippet of the item.
     */
    public QuakeItem( double lat, double lng , String title, String snippet )
    {
        this.position = new LatLng(lat, lng);
        this.title = title;
        this.snippet = snippet;
    }

    @Override
    public LatLng getPosition()
    {
        return position;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    @Override
    public String getSnippet()
    {
        return snippet;
    }
}
