package com.wagter.joris.wagterweer.util;

import com.wagter.joris.wagterweer.model.Weather;

/**
 * Interface that allows the setting of a Weather object.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface WeatherAware
{
    /**
     * Set the Weather object.
     *
     * @param weather the Weather object.
     */
    void setWeather( Weather weather );
}
