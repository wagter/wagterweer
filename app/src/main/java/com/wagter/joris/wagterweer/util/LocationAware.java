package com.wagter.joris.wagterweer.util;

import android.location.Location;

/**
 * Implement in class to make objects aware of a location.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface LocationAware
{
    /**
     * Set a location to the object.
     *
     * @param location the location to make the object aware of.
     */
    void setLocation( Location location );
}
