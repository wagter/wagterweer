package com.wagter.joris.wagterweer.converter;

import java.util.HashMap;

/**
 * A class to translate weekday abbreviations from English to Dutch.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class DayOfWeekConverter
{
    /**
     * The translation table.
     */
    private HashMap< String, String > transTable = createTransTable();

    /**
     * Get the translations for a certain abbreviation.
     *
     * @param day the English abbreviation to translate.
     * @return the translated Dutch abbreviation.
     */
    public String get( String day )
    {
        return transTable.get( day );
    }

    /**
     * Create the translation table.
     *
     * @return the newly created translation table.
     */
    private HashMap< String, String > createTransTable()
    {
        return new HashMap< String, String >()
        {{
            put( "Mon", "Ma" );
            put( "Tue", "Di" );
            put( "Wed", "Wo" );
            put( "Thu", "Do" );
            put( "Fri", "Vr" );
            put( "Sat", "Za" );
            put( "Sun", "Zo" );
        }};
    }
}
