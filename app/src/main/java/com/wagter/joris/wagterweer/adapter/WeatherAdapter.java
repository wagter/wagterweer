package com.wagter.joris.wagterweer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.wagter.joris.wagterweer.item.WeatherItem;

import java.util.List;

/**
 * List adapter for WeatherItem layout objects.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class WeatherAdapter extends BaseAdapter
{
    /**
     * The layout inflater.
     */
    private LayoutInflater inflater;

    /**
     * The list of WeatherItem objects.
     */
    private List< WeatherItem > weatherItems;

    /**
     * WeatherAdapter constructor.
     *
     * @param applicationContext the application context.
     * @param weatherItems the list of WeatherItem objects.
     */
    public WeatherAdapter( Context applicationContext, List< WeatherItem > weatherItems )
    {
        this.weatherItems = weatherItems;
        inflater = LayoutInflater.from( applicationContext );
    }

    @Override
    public int getCount()
    {
        return weatherItems.size();
    }

    @Override
    public Object getItem( int i )
    {
        return weatherItems.get( i );
    }

    @Override
    public long getItemId( int i )
    {
        return i;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {

        WeatherItem listItem = (WeatherItem) getItem( position );
        convertView = listItem.inflateView( inflater, convertView, parent, false );

        return convertView;
    }
}