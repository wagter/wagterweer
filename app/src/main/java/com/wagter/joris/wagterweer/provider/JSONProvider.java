package com.wagter.joris.wagterweer.provider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * To provide a JSON object from an URL.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class JSONProvider
{
    /**
     * Get a JSON object from an URL.
     *
     * @param url the URL to get the JSON object from.
     * @return the JSON object created from the response of the URL.
     * @throws IOException if the URL returns no result.
     * @throws JSONException if the JSON returned by the URL is malformed.
     */
    public JSONObject getJSON( URL url ) throws IOException, JSONException
    {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        BufferedReader reader =
                new BufferedReader( new InputStreamReader( connection.getInputStream() ) );
        StringBuilder json = new StringBuilder( 1024 );
        String tmp;
        while ( ( tmp = reader.readLine() ) != null )
            json.append( tmp ).append( "\n" );
        reader.close();

        return new JSONObject( json.toString() );
    }
}
