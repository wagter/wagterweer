package com.wagter.joris.wagterweer.model;

import java.util.Date;

/**
 * Holds data about a single earthquake.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Quake
{
    /**
     * The date of the quake.
     */
    private Date dateTime;

    /**
     * The depth of the quake.
     */
    private double depth;

    /**
     * The evaluation mode of the quake.
     */
    private String evaluationMode;

    /**
     * The latitude of the quake.
     */
    private double lat;

    /**
     * The longitude of the quake.
     */
    private double lng;

    /**
     * The magnitude of the quake.
     */
    private double magnitude;

    /**
     * The place name of the quake.
     */
    private String place;

    /**
     * The type of the quake.
     */
    private String type;

    /**
     * Get the date of the quake.
     *
     * @return the date of the quake.
     */
    public Date getDateTime()
    {
        return dateTime;
    }

    /**
     * Set the date to the quake.
     * @param dateTime the date of the quake.
     */
    public void setDateTime( Date dateTime )
    {
        this.dateTime = dateTime;
    }

    /**
     * Get the depth of the quake.
     *
     * @return the depth of the quake.
     */
    public double getDepth()
    {
        return depth;
    }

    /**
     * Set the depth to the quake.
     *
     * @param depth the depth of the quake.
     */
    public void setDepth( double depth )
    {
        this.depth = depth;
    }

    /**
     * Get the evaluation mode of the quake.
     *
     * @return the evaluation mode of the quake.
     */
    public String getEvaluationMode()
    {
        return evaluationMode;
    }

    /**
     * Set the evaluation mode to the quake.
     *
     * @param evaluationMode the evaluation mode of the quake.
     */
    public void setEvaluationMode( String evaluationMode )
    {
        this.evaluationMode = evaluationMode;
    }

    /**
     * Get the latitude of the quake.
     *
     * @return the latitude of the quake.
     */
    public double getLat()
    {
        return lat;
    }

    /**
     * Set the latitude to the quake.
     *
     * @param lat the latitude of the quake.
     */
    public void setLat( double lat )
    {
        this.lat = lat;
    }

    /**
     * Get the longitude of the quake.
     *
     * @return the longitude of the quake.
     */
    public double getLng()
    {
        return lng;
    }

    /**
     * Set the longitude to the quake.
     *
     * @param lng the longitude of the quake.
     */
    public void setLng( double lng )
    {
        this.lng = lng;
    }

    /**
     * Get the magnitude of the quake.
     *
     * @return the magnitude of the quake.
     */
    public double getMagnitude()
    {
        return magnitude;
    }

    /**
     * Set the magnitude to the quake.
     *
     * @param magnitude the magnitude of the quake.
     */
    public void setMagnitude( double magnitude )
    {
        this.magnitude = magnitude;
    }

    /**
     * Get the place name of the quake.
     *
     * @return the place name of the quake.
     */
    public String getPlace()
    {
        return place;
    }

    /**
     * Set the place name to the quake.
     *
     * @param place the place name of the quake.
     */
    public void setPlace( String place )
    {
        this.place = place;
    }

    /**
     * Get the type of the quake.
     *
     * @return the type of the quake.
     */
    public String getType()
    {
        return type;
    }

    /**
     * Set the type to the quake.
     *
     * @param type the type of the quake.
     */
    public void setType( String type )
    {
        this.type = type;
    }
}
