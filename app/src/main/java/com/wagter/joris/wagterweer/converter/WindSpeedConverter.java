package com.wagter.joris.wagterweer.converter;

import java.util.HashMap;
import java.util.Map;

/**
 * To translate a wind speed number to a Dutch textual representation.
 *
 * @link https://nl.wikipedia.org/wiki/Schaal_van_Beaufort
 */
public class WindSpeedConverter
{
    /**
     * The numeric wind power according to the Beaufort scale.
     */
    private int windPower;

    /**
     * The wind speed translation map.
     */
    private Map< Integer, String > windSpeedMap;

    /**
     * WindSpeedConverter constructor.
     *
     * @param windPower the wind power according to the Beaufort scale.
     */
    public WindSpeedConverter( int windPower )
    {
        this.windPower = windPower;

        windSpeedMap = new HashMap< Integer, String >()
        {{
            put( 0, "windstil" );
            put( 1, "zeer zwakke wind" );
            put( 2, "zwakke wind" );
            put( 3, "vrij matige wind" );
            put( 4, "matige wind" );
            put( 5, "vrij krachtige wind" );
            put( 6, "krachtige wind" );
            put( 7, "harde wind" );
            put( 8, "stormachtige wind" );
            put( 9, "storm" );
            put( 10, "zware storm" );
            put( 11, "zeer zware storm" );
            put( 12, "orkaan" );
        }};
    }

    @Override
    public String toString()
    {
        return windSpeedMap.get( windPower );
    }
}
