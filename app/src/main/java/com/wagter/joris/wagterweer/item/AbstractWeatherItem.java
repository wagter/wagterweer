package com.wagter.joris.wagterweer.item;

import android.content.Context;

import com.wagter.joris.wagterweer.model.Weather;

/**
 * An abstract weather item. Extend this class to create layout elements to display weather info.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public abstract class AbstractWeatherItem implements WeatherItem
{
    /**
     * The weather object.
     */
    protected Weather weather;

    /**
     * The application context.
     */
    protected Context context;

    /**
     * AbstractWeatherItem constructor.
     *
     * @param context the application context.
     * @param weather the weather object.
     */
    AbstractWeatherItem( Context context, Weather weather )
    {
        this.context = context;
        setWeather( weather );
    }

    @Override
    public void setWeather( Weather weather )
    {
        this.weather = weather;
        updateViews();
    }
}
