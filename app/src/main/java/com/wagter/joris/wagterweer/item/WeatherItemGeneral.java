package com.wagter.joris.wagterweer.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wagter.joris.wagterweer.R;
import com.wagter.joris.wagterweer.model.Weather;
import com.wagter.joris.wagterweer.provider.WeatherDrawableProvider;

/**
 * To display general weather information.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class WeatherItemGeneral extends AbstractWeatherItem
{
    /**
     * Image view to display an icon based on the current weather conditions.
     */
    private ImageView iconView;

    /**
     * Text view to display the current temperature.
     */
    private TextView tempView;

    /**
     * Text view to display a description of the current weather conditions.
     */
    private TextView descView;

    /**
     * Provider to provide an icon matching the current weather conditions.
     */
    private WeatherDrawableProvider iconProvider;

    /**
     * WeatherItemGeneral constructor.
     *
     * @param context the application context.
     * @param weather the weather model.
     */
    public WeatherItemGeneral( Context context, Weather weather )
    {
        super( context, weather );
        iconProvider = new WeatherDrawableProvider( context );
    }

    @Override
    public View inflateView( LayoutInflater inflater, View convertView, ViewGroup root, boolean attachToRoot )
    {
        if ( convertView == null ) {
            convertView = inflater.inflate( R.layout.activity_weather_item_general, root, attachToRoot );
        }

        iconView = convertView.findViewById( R.id.weatherIcon );
        tempView = convertView.findViewById( R.id.temperatureText );
        descView = convertView.findViewById( R.id.weatherDescription );

        updateViews();
        return convertView;
    }

    @Override
    public void updateViews()
    {
        if ( weather == null ) {
            return;
        }

        if ( iconView != null ) {
            iconView.setImageDrawable( iconProvider.getIconByWeather( weather ) );
        }

        if ( tempView != null ) {
            tempView.setText(
                    context.getString( R.string.temperature, "" + weather.getTemperature() )
            );
        }

        if ( descView != null ) {
            descView.setText( weather.getDescription() );
        }


    }
}
