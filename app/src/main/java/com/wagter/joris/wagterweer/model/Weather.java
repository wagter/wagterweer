package com.wagter.joris.wagterweer.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Containing data about current weather conditions and forecasts.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Weather
{
    /**
     * The weather condition code.
     */
    private int condition;

    /**
     * Textual description of the weather.
     */
    private String description;

    /**
     * The current temperature.
     */
    private double temperature;

    /**
     * The minimum temperature of the date.
     */
    private double temperatureMin;

    /**
     * The maximum temperature of the date.
     */
    private double temperatureMax;

    /**
     * The current air pressure.
     */
    private int pressure;

    /**
     * The current humidity.
     */
    private int humidity;

    /**
     * The current wind speed.
     */
    private double windSpeed;

    /**
     * The current wind direction.
     */
    private int windDegrees;

    /**
     * The time of sunrise of the place of the weather.
     */
    private Date sunrise;

    /**
     * The time of sunset of the place of the weather.
     */
    private Date sunset;

    /**
     * The place name of the weather's location.
     */
    private String place;

    /**
     * The date / time that the weather is updated.
     */
    private Date date;

    /**
     * The list containing forecast objects.
     */
    private List< Forecast > forecastList;

    /**
     * Weather constructor.
     */
    public Weather()
    {
        forecastList = new ArrayList<>();
    }

    /**
     * Get the current condition code.
     *
     * @return the current condition code.
     */
    public int getCondition()
    {
        return condition;
    }

    /**
     * Set the current condition code.
     *
     * @param condition the current condition code.
     */
    public void setCondition( int condition )
    {
        this.condition = condition;

    }

    /**
     * Get the textual weather description.
     *
     * @return the textual weather description.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Set the textual weather description.
     *
     * @param description the textual weather description.
     */
    public void setDescription( String description )
    {
        this.description = description;
    }

    /**
     * Get the current temperature.
     *
     * @return the current temperature.
     */
    public double getTemperature()
    {
        return temperature;
    }

    /**
     * Set the current temperature.
     *
     * @param temperature the current temperature.
     */
    public void setTemperature( double temperature )
    {
        this.temperature = temperature;
    }

    /**
     * Get the minimum temperature.
     *
     * @return the minimum temperature.
     */
    public double getTemperatureMin()
    {
        return temperatureMin;
    }

    /**
     * Set the minimum temperature.
     *
     * @param temperatureMin the minimum temperature.
     */
    public void setTemperatureMin( double temperatureMin )
    {
        this.temperatureMin = temperatureMin;
    }

    /**
     * Get the maximum temperature.
     *
     * @return the maximum temperature.
     */
    public double getTemperatureMax()
    {
        return temperatureMax;
    }

    /**
     * Set the maximum temperature.
     *
     * @param temperatureMax the maximum temperature.
     */
    public void setTemperatureMax( double temperatureMax )
    {
        this.temperatureMax = temperatureMax;
    }

    /**
     * Get the air pressure.
     *
     * @return the air pressure.
     */
    public int getPressure()
    {
        return pressure;
    }

    /**
     * Set the air pressure.
     *
     * @param pressure the air pressure.
     */
    public void setPressure( int pressure )
    {
        this.pressure = pressure;
    }

    /**
     * Get the humidity.
     *
     * @return the humidity.
     */
    public int getHumidity()
    {
        return humidity;
    }

    /**
     * Set the humidity.
     *
     * @param humidity the humidity.
     */
    public void setHumidity( int humidity )
    {
        this.humidity = humidity;
    }

    /**
     * Get the wind speed.
     *
     * @return the wind speed.
     */
    public double getWindSpeed()
    {
        return windSpeed;
    }

    /**
     * Set the wind speed.
     *
     * @param windSpeed the wind speed.
     */
    public void setWindSpeed( double windSpeed )
    {
        this.windSpeed = windSpeed;
    }

    /**
     * Get the wind direction.
     *
     * @return the wind direction.
     */
    public int getWindDegrees()
    {
        return windDegrees;
    }

    /**
     * Set the wind direction.
     *
     * @param windDegrees the wind direction.
     */
    public void setWindDegrees( int windDegrees )
    {
        this.windDegrees = windDegrees;
    }

    /**
     * Get the sunrise time.
     *
     * @return the sunrise time.
     */
    public Date getSunrise()
    {
        return sunrise;
    }

    /**
     * Set the sunrise time.
     *
     * @param sunrise the sunrise time.
     */
    public void setSunrise( Date sunrise )
    {
        this.sunrise = sunrise;
    }

    /**
     * Get the sunset time.
     *
     * @return the sunset time.
     */
    public Date getSunset()
    {
        return sunset;
    }

    /**
     * Set the sunset time.
     *
     * @param sunset the sunset time.
     */
    public void setSunset( Date sunset )
    {
        this.sunset = sunset;
    }

    /**
     * Get the place name.
     *
     * @return the place name.
     */
    public String getPlace()
    {
        return place;
    }

    /**
     * Set the place name.
     *
     * @param place the place name.
     */
    public void setPlace( String place )
    {
        this.place = place;
    }

    /**
     * Get the date.
     *
     * @return the date.
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * Set the date.
     *
     * @param date the date.
     */
    public void setDate( Date date )
    {
        this.date = date;
    }

    /**
     * Add a forecast to the list.
     *
     * @param forecast the forecast to add.
     */
    public void addForecast( Forecast forecast )
    {
        forecastList.add( forecast );
    }

    /**
     * Get a forecast from the list by index.
     *
     * @param index the index of the forecast list.
     *
     * @return the forecast selected from the list.
     */
    public Forecast getForecast( int index )
    {
        return forecastList.get( index );
    }

    /**
     * Get the forecast list.
     *
     * @return the forecast list.
     */
    public List< Forecast > getForecastList()
    {
        return forecastList;
    }

    /**
     * Set the forecast list.
     *
     * @param forecastList the forecast list.
     */
    public void setForecastList( List< Forecast > forecastList )
    {
        this.forecastList = forecastList;
    }

    @Override
    public String toString()
    {
        JSONObject jsonObject = new JSONObject()
        {{
            try {
                put( "condition", getCondition() );
                put( "description", getDescription() );
                put( "temperature", getTemperature() );
                put( "temperatureMin", getTemperatureMin() );
                put( "temperatureMax", getTemperatureMax() );
                put( "pressure", getPressure() );
                put( "humidity", getHumidity() );
                put( "windSpeed", getWindSpeed() );
                put( "windDegrees", getWindDegrees() );
                put( "sunrise", getSunrise() );
                put( "sunset", getSunset() );
                put( "place", getPlace() );
            } catch ( JSONException e ) {
                e.printStackTrace();
            }


        }};

        return jsonObject.toString();
    }
}
