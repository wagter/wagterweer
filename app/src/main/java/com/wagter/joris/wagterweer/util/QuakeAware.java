package com.wagter.joris.wagterweer.util;

import com.wagter.joris.wagterweer.model.Quake;

import java.util.List;

/**
 * To implement make objects aware of quakes.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface QuakeAware
{
    /**
     * Set a list of quakes to the object.
     *
     * @param quakes the list of quakes.
     */
    void setQuakes( List<Quake> quakes );

    /**
     * Get the list of quakes from the object.
     *
     * @return the list of quakes from the object.
     */
    List<Quake> getQuakes();
}
