package com.wagter.joris.wagterweer.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wagter.joris.wagterweer.R;
import com.wagter.joris.wagterweer.adapter.WeatherAdapter;
import com.wagter.joris.wagterweer.dialog.TermsDialog;
import com.wagter.joris.wagterweer.util.CompareLocations;
import com.wagter.joris.wagterweer.util.LocationAware;
import com.wagter.joris.wagterweer.model.Weather;
import com.wagter.joris.wagterweer.item.WeatherItem;
import com.wagter.joris.wagterweer.item.WeatherItemForecast;
import com.wagter.joris.wagterweer.item.WeatherItemGeneral;
import com.wagter.joris.wagterweer.item.WeatherItemWind;
import com.wagter.joris.wagterweer.provider.LocationProvider;
import com.wagter.joris.wagterweer.util.WeatherAware;
import com.wagter.joris.wagterweer.provider.WeatherDrawableProvider;
import com.wagter.joris.wagterweer.provider.WeatherProvider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * MainActivity of WagterWeer app.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class MainActivity extends AppCompatActivity implements LocationAware, WeatherAware
{
    /**
     * The current location to use to get weather data
     */
    private Location location;

    /**
     * The weather provider used to get weather data from API
     */
    private WeatherProvider weatherProvider;

    /**
     * The provider to provide the current location.
     */
    private LocationProvider locationProvider;

    /**
     * The provider used to provide images / icons matching weather conditions.
     */
    private WeatherDrawableProvider iconProvider;

    /**
     * Header image view
     */
    private ImageView headerImageView;

    /**
     * Header text views.
     */
    private TextView placeNameView, dateUpdatedView;

    /**
     * The object containing the weather conditions.
     */
    private Weather weather;

    /**
     * The list containing weather aware objects.
     */
    private List< WeatherItem > weatherItems;

    /**
     * The swipe to refresh layout element.
     */
    private SwipeRefreshLayout swipeRefreshLayout;

    /**
     * MainActivity constructor.
     */
    public MainActivity()
    {
        this.locationProvider = new LocationProvider( this, this );
        this.weatherProvider = new WeatherProvider( this, this );
        this.iconProvider = new WeatherDrawableProvider( this );

    }

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        if ( savedInstanceState != null ) {
            return;
        }

        TermsDialog termsDialog = new TermsDialog( this );
        termsDialog.show();

        weather = weatherProvider.loadFromCache();

        ListView weatherListView = findViewById( R.id.weatherList );

        weatherItems = new ArrayList< WeatherItem >()
        {{
            add( new WeatherItemGeneral( getApplicationContext(), weather ) );
            add( new WeatherItemWind( getApplicationContext(), weather ) );
            add( new WeatherItemForecast( getApplicationContext(), weather ) );
        }};

        WeatherAdapter weatherAdapter = new WeatherAdapter( getApplicationContext(), weatherItems );
        weatherListView.setAdapter( weatherAdapter );

        swipeRefreshLayout = findViewById( R.id.swiperefresh );
        swipeRefreshLayout.setOnRefreshListener(this::updateWeather);

        setSupportActionBar( createToolbar() );

        headerImageView = findViewById( R.id.headerImage );
        placeNameView = findViewById( R.id.placeName );
        dateUpdatedView = findViewById( R.id.dateUpdated );

        if ( weather != null ) {
            setWeather( weather );
        }

        locationProvider.requestUpdate();
    }

    /**
     * Update the weather model from the API
     */
    private void updateWeather()
    {
        if ( swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing() ) {
            swipeRefreshLayout.setRefreshing( true );
        }

        weatherProvider.requestUpdate( location, swipeRefreshLayout );
    }


    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.menu_main, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch ( item.getItemId() ) {
            case R.id.action_refresh:
                if ( location != null ) {
                    updateWeather();
                }
                break;
            case R.id.action_location:
                locationProvider.requestUpdate();
                break;
            case R.id.quakes:
                startQuakesActivity();
            default:
                break;
        }
        return true;
    }

    @Override
    public void setLocation( Location location )
    {
        if ( location != null ) {
            boolean isDifferent = new CompareLocations( this.location, location ).different();
            if ( this.location == null || isDifferent ) {
                this.location = location;
            }
            updateWeather();
        }
    }

    @Override
    public void setWeather( Weather weather )
    {
        SimpleDateFormat df = new SimpleDateFormat( "dd-M-yyy H:m", Locale.ENGLISH );
        dateUpdatedView.setText( df.format( weather.getDate() ) );
        placeNameView.setText( weather.getPlace() );
        headerImageView.setImageDrawable( iconProvider.getImageByWeather(weather) );
        for ( WeatherItem listItem : weatherItems ) {
            listItem.setWeather( weather );
        }
    }

    /**
     * Create a toolbar without title.
     *
     * @return the newly created toolbar.
     */
    private Toolbar createToolbar()
    {
        Toolbar toolbar = findViewById( R.id.menu_main );
        toolbar.setTitle( "" );

        return toolbar;
    }

    /**
     * Start the earthquake activity.
     */
    private void startQuakesActivity()
    {
        Intent intent = new Intent( this,  QuakeActivity.class);
        startActivity( intent );
    }
}
